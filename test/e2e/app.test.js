import path from 'path'

import { Application } from 'spectron'
import electronPath from 'electron'

const APP_START_TIMEOUT = 10000
jest.setTimeout(APP_START_TIMEOUT)

const appPath = path.join(__dirname, '../../app')
const app = new Application({
  path: electronPath,
  args: [appPath, 'wipefull'],
  chromeDriverLogPath: path.join(__dirname, '../chromedriver.log'),
  webdriverLogPath: path.join(__dirname, '../webdriver.log'),
})

beforeAll(() => app.start(), APP_START_TIMEOUT)
afterAll(() => app.stop())

describe('Zen Protocol Wallet', () => {
  describe('first time postinstall user journey', () => {
    beforeAll(async () => {
      const isVisible = await app.browserWindow.isVisible()
      expect(isVisible).toBe(true)
    })

    it('shows the onboarding sequence', async () => {
      const { client } = app
      await client.waitUntilTextExists('.body h1', 'What is Zen Protocol?')
      client.click('.button-on-right')
      await client.waitUntilTextExists('.body h1', 'How does the ZP wallet work?')
      client.click('.button-on-right')
      await client.waitUntilTextExists('.body h1', 'Zen Protocol Seed & Passwords')
      client.click('.button-on-right')
      await client.waitUntilTextExists('.body h1', 'Only YOU are in control')
      client.click('.button-on-right')
      await client.waitUntilTextExists('.body h1', 'Setting Up Your Wallet')
      await client.waitUntilTextExists('.create-wallet a', 'Create Wallet')
      await client.waitUntilTextExists('.import-wallet a', 'Import Wallet')
      client.click('.import-wallet a')
      await client.waitUntilTextExists('.body h1', 'Import Your Mnemonic Passphrase')
      for (let i = 0; i < 24; i += 1) {
        client.setValue(`.passphrase-quiz input[data-index='${i}']`, 'top')
      }
      await client.waitForEnabled('.button-on-right')
      client.click('.button-on-right')

      await client.waitUntilTextExists('.body h1', 'Create a password')
      client.setValue(`input[name=${'password'}]`, 'test')
      client.setValue(`input[name=${'password-confirmation'}]`, 'test')
      await client.waitForEnabled('.button-on-right')
      client.click('.button-on-right')

      await client.waitUntilTextExists('.body h1', 'Terms and Conditions')
      client.click('.checkbox-text')
      await client.waitForEnabled('.button-on-right')
      client.click('.button-on-right')

      await client.waitUntilTextExists('.page-title h1', 'Portfolio')
    })
  })
})
